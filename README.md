# STM32F302 Examples

## Features

- This project provides rust examples for my STM32F302R8
  - blink is as it says on the tin
  - blink2 is the same but with 3 LEDs
  - Interrupt is how to use interrupts for a GPIO pin

## Building
```
cargo build --release
```

## Flashing
```
cargo flash --bin blinky --release --chip stm32f302RETx --connect-under-reset
```
