// This is a simple example that blinks an LED on pin PB13.

#![deny(unsafe_code)]
#![no_main]
#![no_std]
use cortex_m_rt::entry;
use stm32f3xx_hal as hal;
use hal::{ pac, prelude::* };

use panic_probe as _;

#[entry]
fn main() -> ! {
    if let Some(dp) =  pac::Peripherals::take() {

        // Set up the system clock.
        let mut rcc = dp.RCC.constrain();

        // Get the GPIOB peripheral
        let mut gpiob = dp.GPIOB.split(&mut rcc.ahb);

        // (Re-)configure PE13 as output
        let mut led = gpiob.pb13.into_push_pull_output(&mut gpiob.moder, &mut gpiob.otyper);

        led.set_low().unwrap();

        loop {
            led.toggle().unwrap();

            // Wait for 1_000_000 cycles
            cortex_m::asm::delay(8_000_000);

            if led.is_set_low().unwrap() {
                led.set_high().unwrap();
            } else {
                led.set_low().unwrap();
            }

            // Wait for 1_000_000 cycles
            cortex_m::asm::delay(8_000_000);
        }
    }

    loop {
        cortex_m::asm::nop();
    }

}
