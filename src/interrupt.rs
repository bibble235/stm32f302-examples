#![no_std]
#![no_main]

use core::cell::RefCell;

use cortex_m::peripheral::NVIC;
use cortex_m_rt::entry;
use critical_section::Mutex;

use stm32f3xx_hal as hal;
use hal::{ gpio::{ self, Edge, Input, Output, PushPull }, interrupt, pac, prelude::* };

use panic_probe as _;

// Create Mutex for PB10 Led
type LedPin = gpio::PB10<Output<PushPull>>;
static LED10: Mutex<RefCell<Option<LedPin>>> = Mutex::new(RefCell::new(None));

// Create Mutex for PC13 Button
type ButtonPin = gpio::PC13<Input>;
static BUTTON_PC13: Mutex<RefCell<Option<ButtonPin>>> = Mutex::new(RefCell::new(None));

#[entry]
fn main() -> ! {
    if
        let (Some(dp), Some(_cp)) = (
            pac::Peripherals::take(),
            cortex_m::peripheral::Peripherals::take(),
        )
    {
        // Set up the system clock.
        let mut rcc = dp.RCC.constrain();

        // Promote SYSCFG structure to HAL to be able to configure interrupts
        let mut syscfg = dp.SYSCFG.constrain(&mut rcc.apb2);

        // Get the EXTI
        let mut exti = dp.EXTI;

        // Promote the GPIOB PAC struct
        let mut gpiob = dp.GPIOB.split(&mut rcc.ahb);

        // Promote the GPIOC PAC struct
        let gpioc = dp.GPIOC.split(&mut rcc.ahb);

        // (Re-)configure PB10 as output
        let led10 = gpiob.pb10.into_push_pull_output(&mut gpiob.moder, &mut gpiob.otyper);

        // On the NUCLEO STM32F302R8 USER B1 button connected PC13, Configure Pin and Obtain Handle
        let mut button = gpioc.pc13;

        // Make button an interrupt source
        syscfg.select_exti_interrupt_source(&button);

        // Set when to trigger
        button.trigger_on_edge(&mut exti, Edge::Rising);

        // Enable the interrupt for button
        button.enable_interrupt(&mut exti);

        // Get the interrupt number
        let interrupt_num = button.interrupt();

        // Store the button and led in the global static variables
        critical_section::with(|cs| {
            *BUTTON_PC13.borrow(cs).borrow_mut() = Some(button);
            *LED10.borrow(cs).borrow_mut() = Some(led10);
        });

        // Unmask the interrupt
        unsafe {
            NVIC::unmask(interrupt_num);
        }
    }

    loop {
        // Wait for an interrupt
        cortex_m::asm::wfi();
    }
}

#[interrupt]
fn EXTI15_10() {
    critical_section::with(|cs| {
        // Toggle the LED
        LED10.borrow(cs).borrow_mut().as_mut().unwrap().toggle().unwrap();

        // Clear the interrupt pending bit so we don't infinitely call this routine
        BUTTON_PC13.borrow(cs).borrow_mut().as_mut().unwrap().clear_interrupt();
    })
}