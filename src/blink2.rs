// This is a simple example that blinks three LEDs in a sequence.
// The LEDs are connected to pins PA8, PB10 and PB13.
// The LEDs are toggled using the `toggle` method.
// The delay between toggles is created using the `delay` method.

#![deny(unsafe_code)]
#![no_main]
#![no_std]
use cortex_m_rt::entry;
use stm32f3xx_hal as hal;
use hal::{ pac, prelude::* };

use panic_probe as _;

#[entry]
fn main() -> ! {
    if let Some(dp) =  pac::Peripherals::take() {

        // Set up the system clock.
        let mut rcc = dp.RCC.constrain();

        // Get the GPIOA peripheral
        let mut gpioa = dp.GPIOA.split(&mut rcc.ahb);

        // Get the GPIOB peripheral
        let mut gpiob = dp.GPIOB.split(&mut rcc.ahb);

        // (Re-)configure PA8 as output
        let mut led8 = gpioa.pa8.into_push_pull_output(&mut gpioa.moder, &mut gpioa.otyper);

        // (Re-)configure PB10 as output
        let mut led10 = gpiob.pb10.into_push_pull_output(&mut gpiob.moder, &mut gpiob.otyper);

        // (Re-)configure PB13 as output
        let mut led13 = gpiob.pb13.into_push_pull_output(&mut gpiob.moder, &mut gpiob.otyper);

        led8.set_low().unwrap();
        led10.set_low().unwrap();
        led13.set_low().unwrap();

        loop {
            led8.toggle().unwrap();
            led10.toggle().unwrap();
            led13.toggle().unwrap();

            // Wait for 1_000_000 cycles
            cortex_m::asm::delay(8_000_000);

            if led8.is_set_low().unwrap() {
                led8.set_high().unwrap();
            } else {
                led8.set_low().unwrap();
            }

            if led10.is_set_low().unwrap() {
                led10.set_high().unwrap();
            } else {
                led10.set_low().unwrap();
            }

            if led13.is_set_low().unwrap() {
                led13.set_high().unwrap();
            } else {
                led13.set_low().unwrap();
            }

            // Wait for 1_000_000 cycles
            cortex_m::asm::delay(8_000_000);
        }

    }

    loop {
        cortex_m::asm::nop();
    }

}
